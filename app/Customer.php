<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name_first', 'name_second', 'cpf',
        'rg', 'email', 'phone',
        'address', 'number', 'cep',
        'complement',
        'id_city', 'id_state'
    ];

    protected $dates = [
        'deleted_at'
    ];
}
