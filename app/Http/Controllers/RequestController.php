<?php

namespace App\Http\Controllers;

use App\City;
use App\Provider;

use Illuminate\Http\Request;

class RequestController extends Controller
{
    public function getCities(Request $request)
    {
        $num_state = $request->num_state;

        $data = City::where('id_num_state', $num_state)->get();

        return response()->json($data);
    }

    public function getProviders(Request $request)
    {
        $provider = $request->provider;

        $data = Provider::where('id', $provider)->get();

        return response()->json($data);
    }
}
