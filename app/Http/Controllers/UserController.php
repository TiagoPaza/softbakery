<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\State;
use App\User;

use App\Repositories\ImageRepository;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:Gestão']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $users = User::all();

        foreach ($users as &$user) {
            $user['name_full'] = $user['name_first'] . ' ' . $user['name_second'];
        }

        return view('pages/users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $states = State::all();
        $roles = Role::all();

        return view('pages/users.create', compact('states', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @param ImageRepository $repo
     * @return Response
     * @throws
     */
    public function store(Request $request, ImageRepository $repo)
    {
        $this->validate($request, [
            'name_first' => 'required|max:191',
            'name_second' => 'required|max:191',
            'cpf' => 'required|unique:users,cpf',
            'rg' => 'required',
            'phone' => 'required|max:191',
            'birth_date' => 'required',
            'email' => 'required|email|unique:users,email',
            'address' => 'required',
            'number' => 'required',
            'complement' => '',
            'cep' => 'required',
            'id_state' => 'required',
            'id_city' => 'required',
            'password' => 'required|min:6|confirmed'
        ]);

        $user = User::create($request->except('photo'));
        $roles = $request->input('roles');

        if (isset($roles)) {
            foreach ($roles as $role) {
                $role_r = Role::where('id', $role)->firstOrFail();
                $user->assignRole($role_r);
            }
        }

        if ($request->hasFile('photo')) {
            $user->photo = $repo->saveImage($request->photo, $user->id, 'users', 500);
        }

        return redirect()->route('users.index')
            ->with('success', 'O usuário: ' . $user->name_first . ' ' . $user->name_second . ' foi inserido com sucesso.');
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return redirect('pages/users');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $states = State::all();
        $roles = Role::all();

        return view('pages/users.edit', compact('user', 'roles', 'states'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @param ImageRepository $repo
     * @return Response
     * @throws
     */
    public function update(Request $request, $id, ImageRepository $repo)
    {
        $user = User::findOrFail($id);

        $this->validate($request, [
            'name_first' => 'max:191',
            'name_second' => 'max:191',
            'cpf' => '',
            'rg' => '',
            'phone' => 'max:191',
            'birth_date' => '',
            'email' => 'email',
            'address' => '',
            'number' => '',
            'complement' => '',
            'cep' => '',
            'id_state' => '',
            'id_city' => '',
            'password' => 'min:6|confirmed'
        ]);

        $roles = $request->input('roles');
        $input = $request->except('photo');

        if ($request->hasFile('photo')) {
            $user->photo = $repo->saveImage($request->photo, $user->id, 'users', 500);
        }

        $user->fill($input)->save();

        if (isset($roles)) {
            $user->roles()->sync($roles);
        } else {
            $user->roles()->detach();
        }

        return redirect()->route('users.index')
            ->with('success', 'O usuário: ' . $user->name_first . ' ' . $user->name_second . ' foi atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('users.index')
            ->with('success', 'O usuário: ' . $user->name_first . ' ' . $user->name_second . ' foi deletado com sucesso.');
    }
}
