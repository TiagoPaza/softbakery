<?php

namespace App\Http\Controllers;


use App\OrderProduct;
use App\ProductStock;
use Auth;
use Session;

use App\Order;
use App\Product;
use App\Customer;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Spatie\Permission\Models\Role;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:Gestão|Administração']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $orders = Order::all();

        return view('pages/orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $customers = Customer::all();
        $products = Product::all();

        return view('pages/orders.create', compact('customers', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     * @throws
     */
    public function store(Request $request)
    {
        $products = $request->input('id_product');
        $quantity = $request->input('quantity');

        $order = Order::create([
            'code' => substr(uniqid(mt_rand(), true), 0, 8),
            'id_customer' => $request->input('id_customer')
        ]);

        foreach($products as $key => $value) {
            $data[] = array('product' => $value, 'quantity' => $quantity[$key]);
        }

        foreach ($data as $key => $item) {
            $stock = ProductStock::where('id_product', $item['product'])->first();

            if ($stock->items_current >= $item['quantity']) {
                $updateStock = ProductStock::where('id_product', $item['product']);
                $updateStock->update([
                    'items_current' => $stock->items_current - $item['quantity']
                ]);

                $orderProducts = OrderProduct::create([
                    'quantity' => $item['quantity'],
                    'id_product' => $item['product'],
                    'id_order' => $order->id,
                ]);
            } else {
                return redirect()->route('orders.index')
                    ->with('warning', 'Não foi possivel concluir o pedido pois não há estoque disponível.');
            }
        }

        return redirect()->route('orders.index')
            ->with('success', 'O pedido: #' . $order->id . ' foi inserido com sucesso.');
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);

        return view('pages/orders.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $order = Order::findOrFail($id);
        $customers = Customer::all();
        $products = Product::all();

        return view('pages/orders.edit', compact('order', 'customers', 'products'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);

        $this->validate($request, [
            'total_value' => 'regex:/^\d+(\.\d{1,2})?$/',
            'id_customer' => 'integer'
        ]);

        $input = $request->all();

        $order->fill($input)->save();

        return redirect()->route('orders.index')
            ->with('success', 'O pedido: #' . $order->id . ' foi atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        $order->forceDelete();

        return redirect()->route('orders.index')
            ->with('success', 'O pedido: #' . $order->id . ' foi deletado com sucesso.');
    }
}
