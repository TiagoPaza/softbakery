<?php

namespace App\Http\Controllers;

use App\State;
use Auth;
use Session;

use App\Provider;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProviderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:Gestão|Administração']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $providers = Provider::all();

        return view('pages/providers.index', compact('providers'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $states = State::all();

        return view('pages/providers.create', compact('states'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     * @throws
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'cnpj' => 'required|string|max:191',
            'state_registration' => 'required|string|max:191',
            'email' => 'required|email|unique:providers,email',
            'address' => 'required|string',
            'number' => 'required|integer',
            'complement' => '',
            'cep' => 'required|string',
            'id_state' => 'required|integer',
            'id_city' => 'required|integer'
        ]);

        $provider = Provider::create($request->except('photo'));

        return redirect()->route('providers.index')
            ->with('success', 'O fornecedor: #' . $provider->id . ' foi inserido com sucesso.');
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return redirect('pages/providers');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $provider = Provider::findOrFail($id);
        $states = State::all();

        return view('pages/providers.edit', compact('provider', 'states'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws
     */
    public function update(Request $request, $id)
    {
        $provider = Provider::findOrFail($id);

        $this->validate($request, [
            'name' => 'string|max:191',
            'cnpj' => 'string|max:191',
            'state_registration' => 'string|max:191',
            'email' => 'email',
            'address' => 'string',
            'number' => 'integer',
            'complement' => '',
            'cep' => 'string',
            'id_state' => 'integer',
            'id_city' => 'integer'
        ]);

        $input = $request->all();

        $provider->fill($input)->save();

        return redirect()->route('providers.index')
            ->with('success', 'O fornecedor: #' . $provider->id . ' foi atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $provider = Provider::findOrFail($id);
        $provider->forceDelete();

        return redirect()->route('providers.index')
            ->with('success', 'O fornecedor: #' . $provider->id . ' foi deletado com sucesso.');
    }
}
