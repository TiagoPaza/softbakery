<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\Provider;
use App\Customer;

use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $providers = Provider::all()->count();
        if (!isset($providers)) $providers = 0;

        $customers = Customer::all()->count();
        if (!isset($customers)) $customers = 0;

        $orders = Order::all()->count();
        if (!isset($orders)) $orders = 0;

        $products = Product::all()->count();
        if (!isset($products)) $products = 0;


        return view('pages/home', compact('providers', 'customers','orders', 'products'));
    }
}
