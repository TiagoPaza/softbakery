<?php

namespace App\Http\Controllers;

use App\ProductStock;
use Auth;
use Session;

use App\Product;
use App\Provider;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:Gestão|Administração']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $products = Product::all();

        return view('pages/products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $providers = Provider::all();

        return view('pages/products.create', compact('providers'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     * @throws
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'id_provider' => 'required|integer|exists:providers,id',
            'description' => 'required|string|min:3',
            'price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'items_total' => 'required|integer|min:0',
            'items_current' => 'required|integer|min:0'
        ]);

        $product = Product::create([
            'name' => $request->input('name'),
            'id_provider' => $request->input('id_provider'),
            'description' => $request->input('description'),
            'price' => $request->input('price')
        ]);

        $productStock = ProductStock::create([
            'id_product' => $product->id,
            'items_total' => $request->input('items_total'),
            'items_current' => $request->input('items_current')
        ]);

        return redirect()->route('products.index')
            ->with('success', 'O produto: #' . $product->id . ' foi inserido com sucesso.');
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return redirect('pages/products');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $productStock = ProductStock::where('id_product', $id)->first();
        $providers = Provider::all();

        return view('pages/products.edit', compact('product', 'productStock', 'providers'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        $this->validate($request, [
            'name' => 'string|max:191',
            'id_provider' => 'integer|exists:providers,id',
            'description' => 'string|min:3',
            'price' => 'regex:/^\d+(\.\d{1,2})?$/',
            'items_total' => 'integer|min:0',
            'items_current' => 'integer|min:0',
        ]);

        $input = $request->all();

        $product->fill($input)->save();

        return redirect()->route('products.index')
            ->with('success', 'O produto: #' . $product->id . ' foi atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->forceDelete();

        return redirect()->route('products.index')
            ->with('success', 'O produto: #' . $product->id . ' foi deletado com sucesso.');
    }
}
