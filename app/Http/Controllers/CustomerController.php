<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\State;
use App\Customer;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:Gestão|Administração']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $customers = Customer::all();

        return view('pages/customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $states = State::all();

        return view('pages/customers.create', compact('states'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     * @throws
     */
    public function store(Request $request)
    {
        $this->validate ($request, [
            'name_first' => 'required|string|max:191',
            'name_second' => 'required|string|max:191',
            'cpf' => 'required|string|unique:customers,cpf',
            'rg' => 'required|string',
            'phone' => 'required|string|max:191',
            'email' => 'required|email|unique:customers,email',
            'address' => 'required|string',
            'number' => 'required|integer',
            'complement' => '',
            'cep' => 'required|string',
            'id_state' => 'required|integer',
            'id_city' => 'required|integer'
        ]);
        $customer = Customer::create($request->all());

        return redirect()->route('customers.index')
            ->with('success', 'O cliente: ' .  $customer->name_first . ' ' . $customer->name_second  . ' foi inserido com sucesso.');
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return redirect('pages/customers');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $customer = Customer::findOrFail($id);
        $states = State::all();

        return view('pages/customers.edit', compact('customer',  'states'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);

        $this->validate($request, [
            'name_first' => 'max:191',
            'name_second' => 'max:191',
            'cpf' => '',
            'rg' => '',
            'phone' => 'max:191',
            'email' => 'email',
            'address' => '',
            'number' => '',
            'complement' => '',
            'cep' => '',
            'id_state' => '',
            'id_city' => ''
        ]);

        $input = $request->all();

        $customer->fill($input)->save();

        return redirect()->route('customers.index')
            ->with('success', 'O cliente: ' . $customer->name_first . ' ' . $customer->name_second . ' foi atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        $customer->delete();

        return redirect()->route('customers.index')
            ->with('success', 'O cliente: ' . $customer->name_first . ' ' . $customer->name_second . ' foi deletado com sucesso.');
    }
}
