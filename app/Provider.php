<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'cnpj', 'state_registration',
        'email', 'address',
        'number', 'complement', 'cep',
        'id_city', 'id_state'
    ];
}
