<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'id_provider', 'description',
        'price'
    ];

    public function provider()
    {
        return $this->belongsTo(Provider::class, 'id_provider');
    }

    public function stock()
    {
        return $this->hasOne(ProductStock::class, 'id_product');
    }
}
