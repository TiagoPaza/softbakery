<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'total_value', 'id_customer'
    ];

    protected $dates = [
        'deleted_at'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'id_customer');
    }

    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::class, 'id_order');
    }

    public function item($id)
    {
        return Product::where('id', $id)->first();
    }

    public function value_total($id)
    {
        return OrderProduct::select(DB::raw("SUM(products.price * orders_products.quantity) AS total"))->where('id_order', $id)
            ->join('products', 'orders_products.id_product', 'products.id')->first()->total;

    }
}
