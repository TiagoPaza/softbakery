<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsStock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_stock', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('items_total');
            $table->integer('items_current');

            $table->bigInteger('id_product')->unsigned();

            $table->timestamps();
        });

        Schema::table('products_stock', function (Blueprint $table) {
            $table->foreign('id_product')->references('id')
                ->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_stock');
    }
}
