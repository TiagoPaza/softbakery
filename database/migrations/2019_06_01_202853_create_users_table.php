<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_first');
            $table->string('name_second');
            $table->string('cpf');
            $table->string('rg');
            $table->date('birth_date');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone');
            $table->string('address');
            $table->integer('number');
            $table->string('complement')->nullable();
            $table->integer('cep');
            $table->string('photo')->nullable();

            $table->bigInteger('id_city')->unsigned();
            $table->bigInteger('id_state')->unsigned();

            $table->rememberToken();

            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('id_city')->references('id')
                ->on('cities');
            $table->foreign('id_state')->references('id')
                ->on('states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
