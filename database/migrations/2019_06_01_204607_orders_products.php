<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdersProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_product')->unsigned();
            $table->integer('quantity');

            $table->bigInteger('id_order')->unsigned();

            $table->timestamps();
        });

        Schema::table('orders_products', function (Blueprint $table) {
            $table->foreign('id_product')->references('id')
                ->on('products')->onDelete('cascade');
            $table->foreign('id_order')->references('id')
                ->on('orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_products');

    }
}
