<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProviders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('cnpj');
            $table->string('state_registration');
            $table->string('email')->unique();

            $table->string('address');
            $table->integer('number');
            $table->string('complement')->nullable();
            $table->integer('cep');

            $table->bigInteger('id_city')->unsigned();
            $table->bigInteger('id_state')->unsigned();

            $table->timestamps();
        });

        Schema::table('providers', function (Blueprint $table) {
            $table->foreign('id_city')->references('id')
                ->on('cities');
            $table->foreign('id_state')->references('id')
                ->on('states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers');
    }
}
