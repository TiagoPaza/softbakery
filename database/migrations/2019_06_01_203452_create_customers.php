<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_first');
            $table->string('name_second');
            $table->string('cpf');
            $table->string('rg');
            $table->string('phone');
            $table->string('email')->unique();

            $table->string('address');
            $table->integer('number');
            $table->string('complement')->nullable();
            $table->integer('cep');

            $table->bigInteger('id_city')->unsigned();
            $table->bigInteger('id_state')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('customers', function (Blueprint $table) {
            $table->foreign('id_city')->references('id')
                ->on('cities');
            $table->foreign('id_state')->references('id')
                ->on('states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
