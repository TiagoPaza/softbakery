<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
    Route::get('/', 'Auth\LoginController@showLoginForm');
//});

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('pages/home', 'HomeController@index');
    Route::resource('pages/customers', 'CustomerController');
    Route::resource('pages/providers', 'ProviderController');
    Route::resource('pages/products', 'ProductController');
    Route::resource('pages/orders', 'OrderController');
    Route::resource('pages/users', 'UserController');
});
