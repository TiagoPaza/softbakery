@extends('layouts.app')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('pages/home') }}">{{ env('APP_NAME') }} </a></li>
                        <li class="breadcrumb-item"><a href="{{ url('pages/users') }}">Usuários</a></li>
                        <li class="breadcrumb-item active">Editar</li>
                    </ol>
                </nav>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @if ($errors->any())
                <div class="alert bg-danger text-white shadow" role="alert" style="display:block">
                    <div class="card-body">
                        @foreach($errors->all() as $error)
                            <p><i class="fas fa-bullhorn"></i> {{ $error  }}</p>
                        @endforeach
                    </div>
                </div>
            @endif
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h6 class="m-0 font-weight-bold text-primary">Usuários</h6>
                        </div>
                    </div>
                    <div class="card-body">
                        {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT', 'data-toggle' => 'validator', 'novalidate' => 'true', 'enctype' => 'multipart/form-data')) }}
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    {{ Form::label('name_first', 'Nome') }}
                                    {{ Form::text('name_first', null, array('class' => 'form-control', 'required' => '')) }}
                                </div>
                                <div class="form-group col-md-6">
                                    {{ Form::label('name_second', 'Sobrenome') }}
                                    {{ Form::text('name_second', null, array('class' => 'form-control', 'required' => '')) }}
                                </div>
                                <div class="form-group col-md-12">
                                    {{ Form::label('rg', 'RG') }}
                                    {{ Form::text('rg', null, array('class' => 'form-control', 'required' => '')) }}
                                </div>
                                <div class="form-group col-md-10">
                                    {{ Form::label('cpf', 'CPF') }}
                                    {{ Form::text('cpf', null, array('class' => 'form-control cpf', 'required' => '')) }}
                                </div>
                                <div class="form-group col-md-2">
                                    {{ Form::label('birth_date', 'Data de nascimento') }}
                                    {{ Form::date('birth_date', $user->birth_date, array('class' => 'form-control', 'required' => '')) }}
                                </div>
                                <div class="form-group col-md-12">
                                    {{ Form::label('phone', 'Telefone') }}<br>
                                    {{ Form::text('phone', null, array('class' => 'form-control phone', 'required' => '')) }}
                                </div>
                                <div class="form-group col-md-12">
                                    {{ Form::label('email', 'E-mail') }}
                                    {{ Form::email('email', null, array('class' => 'form-control', 'required' => '')) }}
                                </div>
                                <div class="form-group col-md-6">
                                    {{ Form::label('password', 'Senha') }}<br>
                                    {{ Form::password('password', array('class' => 'form-control', 'required' => '')) }}
                                </div>
                                <div class="form-group col-md-6">
                                    {{ Form::label('password', 'Confirmar Senha') }}<br>
                                    {{ Form::password('password_confirmation', array('class' => 'form-control', 'required' => '')) }}
                                </div>
                                <div class="form-group col-md-4">
                                    {{ Form::label('state', 'Estado') }}<br>
                                    <select name="id_state" class="form-control" required="">
                                        @foreach ($states as $item)
                                            <option value="{{ $item->id }}" @if ($user->id_state == $item->id) selected @endif>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-8">
                                    {{ Form::label('city', 'Cidade') }}<br>
                                    <select name="id_city" class="form-control" required=""></select>
                                </div>
                                <div class="form-group col-md-10">
                                    {{ Form::label('address', 'Endereço') }}
                                    {{ Form::text('address', null, array('class' => 'form-control', 'required' => '')) }}
                                </div>
                                <div class="form-group col-md-2">
                                    {{ Form::label('number', 'Número') }}
                                    {{ Form::text('number', null, array('class' => 'form-control', 'required' => '')) }}
                                </div>
                                <div class="form-group col-md-12">
                                    {{ Form::label('complement', 'Complemento') }}
                                    {{ Form::text('complement', null, array('class' => 'form-control')) }}
                                </div>
                                <div class="form-group col-md-12">
                                    {{ Form::label('cep', 'CEP') }}
                                    {{ Form::text('cep', null, array('class' => 'form-control cep', 'required' => '')) }}
                                </div>
                                <div class="form-group col-md-12">
                                    {{ Form::label('role', 'Setor') }}
                                    <div class="form-group">
                                        @foreach ($roles as $role)
                                            {{ Form::checkbox('roles[]',  $role->id ) }}
                                            {{ Form::label($role->name, ucfirst($role->name)) }}<br>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    {{ Form::label('photo', 'Foto') }}<br>
                                    {{ Form::file('photo', array('class' => 'form-control')) }}
                                </div>
                                {{ Form::submit('Editar usuário', array('class' => 'btn btn-primary remove-mask')) }}
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
