@extends('layouts.app')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('pages/home') }}">{{ env('APP_NAME') }} </a></li>
                        <li class="breadcrumb-item"><a href="{{ url('pages/products') }}">Produtos</a></li>
                        <li class="breadcrumb-item active">Editar</li>
                    </ol>
                </nav>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @if ($errors->any())
                <div class="alert bg-danger text-white shadow" role="alert" style="display:block">
                    <div class="card-body">
                        @foreach($errors->all() as $error)
                            <p><i class="fas fa-bullhorn"></i> {{ $error  }}</p>
                        @endforeach
                    </div>
                </div>
            @endif
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h6 class="m-0 font-weight-bold text-primary">Produtos</h6>
                        </div>
                    </div>
                    <div class="card-body">
                        {{ Form::model($product, array('route' => array('products.update', $product->id), 'method' => 'PUT', 'data-toggle' => 'validator', 'novalidate' => 'true', 'enctype' => 'multipart/form-data')) }}
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    {{ Form::label('name', 'Nome') }}
                                    {{ Form::text('name', null, array('class' => 'form-control', 'required' => '')) }}
                                </div>
                                <div class="form-group col-md-12">
                                    {{ Form::label('providers', 'Fornecedor') }}<br>
                                    <select name="id_provider" class="form-control" required="">
                                        @foreach ($providers as $item)
                                            <option value="{{ $item->id }}" @if ($product->id_provider == $item->id) selected @endif>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    {{ Form::label('description', 'Descrição') }}
                                    {{ Form::textarea('description', null, array('class' => 'form-control', 'required' => '')) }}
                                </div>
                                <div class="form-group col-md-12">
                                    {{ Form::label('price', 'Preço') }}
                                    {{ Form::text('price', null, array('class' => 'form-control', 'required' => '')) }}
                                </div>
                                <div class="col-md-12"><hr></div>
                                <div class="form-group col-md-6">
                                    {{ Form::label('items_total', 'Quantidade base') }}
                                    {{ Form::text('items_total', $productStock->items_total, array('class' => 'form-control', 'required' => '')) }}
                                </div>
                                <div class="form-group col-md-6">
                                    {{ Form::label('items_current', 'Quantidade atual') }}
                                    {{ Form::text('items_current', $productStock->items_current, array('class' => 'form-control', 'required' => '')) }}
                                </div>
{{--                                <div class="form-group col-md-12">--}}
{{--                                    {{ Form::label('photo', 'Foto') }}<br>--}}
{{--                                    {{ Form::file('photo', array('class' => 'form-control')) }}--}}
{{--                                </div>--}}
                                {{ Form::submit('Editar produto', array('class' => 'btn btn-primary')) }}
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
