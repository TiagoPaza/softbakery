@extends('layouts.app')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('pages/home') }}">{{ env('APP_NAME') }} </a></li>
                    <li class="breadcrumb-item"><a href="{{ url('pages/products') }}">Produtos</a></li>
                    <li class="breadcrumb-item active">Adicionar</li>
                </ol>
            </nav>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @if ($errors->any())
            <div class="alert bg-danger text-white shadow" role="alert" style="display:block">
                <div class="card-body">
                    @foreach($errors->all() as $error)
                        <p><i class="fas fa-bullhorn"></i> {{ $error  }}</p>
                    @endforeach
                </div>
            </div>
        @endif
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h6 class="m-0 font-weight-bold text-primary">Produtos</h6>
                    </div>
                </div>
                <div class="card-body">
                    {{ Form::open(array('url' => 'pages/products', 'data-toggle' => 'validator', 'novalidate' => 'true', 'enctype' => 'multipart/form-data')) }}
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="name">Nome <span class="text-danger">*</span></label>
                                {{ Form::text('name', '', array('class' => 'form-control', 'required' => '')) }}
                            </div>
                            <div class="form-group col-md-12">
                                <label for="id_provider">Fornecedor <span class="text-danger">*</span></label>
                                <select name="id_provider" class="form-control" required="">
                                    @foreach ($providers as $item)
                                        <option value="{{ $item->id }}"> {{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="description">Descrição <span class="text-danger">*</span></label>
                                {{ Form::textarea('description', '', array('class' => 'form-control', 'required' => '')) }}
                            </div>
                            <div class="form-group col-md-12">
                                <label for="price">Preço <span class="text-danger">*</span></label>
                                {{ Form::text('price', '', array('class' => 'form-control', 'required' => '')) }}
                            </div>
                            <div class="col-md-12"><hr></div>
                            <div class="form-group col-md-6">
                                <label for="items_total">Quantidade base <span class="text-danger">*</span></label>
                                {{ Form::text('items_total', null, array('class' => 'form-control', 'required' => '')) }}
                            </div>
                            <div class="form-group col-md-6">
                                <label for="items_current">Quantidade atual <span class="text-danger">*</span></label>
                                {{ Form::text('items_current', null, array('class' => 'form-control', 'required' => '')) }}
                            </div>
                            {{ Form::submit('Adicionar produto', array('class' => 'btn btn-success')) }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
