@extends('layouts.app')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @if (Session::has('success'))
                <div class="alert bg-success text-white shadow" role="alert" style="display:block">
                    <div class="card-body">
                        <i class="fas fa-bullhorn"></i> {{ Session::get('success') }}</a>
                    </div>
                </div>
            @endif
            @if (Session::has('warning'))
                <div class="alert bg-warning text-white shadow" role="alert" style="display:block">
                    <div class="card-body">
                        <i class="fas fa-bullhorn"></i> {{ Session::get('warning') }}</a>
                    </div>
                </div>
            @endif
            @if ($errors->any())
            <div class="alert bg-danger text-white shadow" role="alert" style="display:block">
                <div class="card-body">
                    <i class="fas fa-bullhorn"></i> Houve um erro ao realizar a ação solicitada.</a>
                </div>
            </div>
        @endif
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Pedidos</h6>
                </div>
                <div class="card-body">
                    <p><a href="{{ url('pages/orders/create') }}" class="btn btn-info btn-rounded">+ Novo pedido</a></p>
                    <div class="table-responsive">
                        <table id="orders" class="table table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>CÓDIGO DO PEDIDO</th>
                                <th>CLIENTE</th>
                                <th>CADASTRADO EM</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td><a href="{{ url('pages/orders/' . $item->id) }}"> {{ $item->code }}</a></td>
                                    <td>{{ $item->customer->name_first . ' ' .  $item->customer->name_second }}</td>
                                    <td>{{ $item->created_at->format('d/m/Y') }} às {{ $item->created_at->format('h:m:s') }}</td>
                                    <td>
                                        {{ Form::open(['method' => 'DELETE', 'route' => ['orders.destroy', $item->id], 'style' => 'display:inline']) }}
                                        {{ Form::button('<i class="fas fa-trash-alt" aria-hidden="true"></i>', array('type' => 'submit', 'class' => 'btn btn-danger btn-outline btn-circle m-r-5', 'title' => 'Deletar')) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
