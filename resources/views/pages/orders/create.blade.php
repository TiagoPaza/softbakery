@extends('layouts.app')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('pages/home') }}">{{ env('APP_NAME') }} </a></li>
                    <li class="breadcrumb-item"><a href="{{ url('pages/orders') }}">Pedidos</a></li>
                    <li class="breadcrumb-item active">Adicionar</li>
                </ol>
            </nav>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @if ($errors->any())
        <div class="alert bg-danger text-white shadow" role="alert" style="display:block">
            <div class="card-body">
                @foreach($errors->all() as $error)
                    <p><i class="fas fa-bullhorn"></i> {{ $error  }}</p>
                @endforeach
            </div>
        </div>
        @endif
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h6 class="m-0 font-weight-bold text-primary">Pedidos</h6>
                    </div>
                </div>
                <div class="card-body">
                    {{ Form::open(array('url' => 'pages/orders', 'data-toggle' => 'validator', 'novalidate' => 'true')) }}
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="id_customer">Cliente <span class="text-danger">*</span></label>
                                <select name="id_customer" class="form-control" required="">
                                    @foreach ($customers as $item)
                                        <option value="{{ $item->id }}"> {{ $item->name_firtst  . ' ' . $item->name_second}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="card-header col-md-12" style="margin-bottom: 15px;">
                                <h6 class="m-0 font-weight-bold">Produtos</h6>
                                <div class="col-md-12">
                                    <div class="card form-product border-left-primary " style="margin: 15px 0px 15px 0px;padding: 15px;">
                                        <div class="form-group" id="form-product-id" data-product="[]">
                                            <div class="input-group">
                                                <span class="increment">1</span>
                                                <div class="form-group col-md-12">
                                                    <label for="id_product">Produto <span class="text-danger">*</span></label>
                                                    <select name="id_product[]" class="form-control" required="">
                                                        @foreach ($products as $item)
                                                            <option value="{{ $item->id }}"> {{ $item->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label for="quantity">Quantidade <span class="text-danger">*</span></label>
                                                    {{ Form::text('quantity[]', '', array('class' => 'form-control', 'required' => '')) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                {{ Form::button('+ Novo produto', array('id' => 'insert-product', 'class' => 'btn btn-primary')) }}
                                {{ Form::button('- Remover produto', array('id' => 'remove-product', 'class' => 'btn btn-danger')) }}
                            </div>
                            {{ Form::submit('Adicionar pedido', array('class' => 'btn btn-success')) }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
