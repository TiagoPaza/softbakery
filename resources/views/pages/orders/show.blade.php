@extends('layouts.app')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('pages/home') }}">{{ env('APP_NAME') }} </a></li>
                        <li class="breadcrumb-item"><a href="{{ url('pages/orders') }}">Pedidos</a></li>
                        <li class="breadcrumb-item active">Visualizar</li>
                    </ol>
                </nav>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @if ($errors->any())
                    <div class="alert bg-danger text-white shadow" role="alert" style="display:block">
                        <div class="card-body">
                            @foreach($errors->all() as $error)
                                <p><i class="fas fa-bullhorn"></i> {{ $error  }}</p>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h6 class="m-0 font-weight-bold text-primary">Pedidos</h6>
                        </div>
                    </div>
                    <div class="card-body">
                        {{ Form::open(array('url' => 'pages/orders', 'data-toggle' => 'validator', 'novalidate' => 'true')) }}
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 text-right    ">
                                    <button class="btn btn-info" id="print" onclick="printContent('print-content');" style="margin-bottom: 10px;"><i class="fas fa-print"></i> Imprimir</button>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="card" id="print-content">
                                        <div class="row" style="padding: 15px;">
                                            <div class="col-sm-12 col-md-6 col-lg-6">
                                                <b>Código:</b> {{ $order->code }}
                                            </div>
                                            <div class="col-sm-12 col-md-6 col-lg-6 text-right">
                                                <b>Venda realizada
                                                    em:</b> {{ date('d/m/Y H:m:s', strtotime($order->created_at)) }}
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12">
                                                <b>Cliente: </b>{{ $order->customer->name_first . ' ' . $order->customer->name_second }}
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12">
                                                <hr>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12">
                                                <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                                                    <h5><b>PRODUTOS</b></h5>
                                                    <table id="order" class="table table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th>NOME</th>
                                                            <th>VALOR UN.</th>
                                                            <th>QUANTIDADE</th>
                                                            <th>TOTAL</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($order->orderProducts as $product)
                                                            <td>{{ $order->item($product->id_product)->name }}</td>
                                                            <td>
                                                                R$ {{ number_format($order->item($product->id_product)->price, 2, ',', '.') }}</td>
                                                            <td>{{ $product->quantity }}</td>
                                                            <td>
                                                                R$ {{ number_format($order->item($product->id_product)->price * $product->quantity, 2, ',', '.') }}</td>
                                                            <tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12">
                                                <hr>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12">
                                                <b>VALOR TOTAL DO
                                                    PEDIDO: </b>R$ {{ number_format($order->value_total($order->id), 2, ',', '.') }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('packages/jquery/jquery.min.js') }}"></script>
    <script>
        function printContent(e) {
            let restorePage = $('body').html();
            let printContent = $('#' + e).clone();

            $('body').empty().html(printContent);
            window.print();

            $('body').html(restorePage);
        }
    </script>
@endsection