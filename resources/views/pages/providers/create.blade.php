@extends('layouts.app')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('pages/home') }}">{{ env('APP_NAME') }} </a></li>
                    <li class="breadcrumb-item"><a href="{{ url('pages/providers') }}">Fornecedores</a></li>
                    <li class="breadcrumb-item active">Adicionar</li>
                </ol>
            </nav>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @if ($errors->any())
            <div class="alert bg-danger text-white shadow" role="alert" style="display:block">
                <div class="card-body">
                    @foreach($errors->all() as $error)
                        <p><i class="fas fa-bullhorn"></i> {{ $error  }}</p>
                    @endforeach
                </div>
            </div>
        @endif
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h6 class="m-0 font-weight-bold text-primary">Fornecedores</h6>
                    </div>
                </div>
                <div class="card-body">
                    {{ Form::open(array('url' => 'pages/providers', 'data-toggle' => 'validator', 'novalidate' => 'true')) }}
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="name">Nome <span class="text-danger">*</span></label>
                                {{ Form::text('name', '', array('class' => 'form-control', 'required' => '')) }}
                            </div>
                            <div class="form-group col-md-12">
                                <label for="cnpj">CNPJ <span class="text-danger">*</span></label>
                                {{ Form::text('cnpj', '', array('class' => 'form-control', 'required' => '')) }}
                            </div>
                            <div class="form-group col-md-12">
                                <label for="state_registration">Inscrição Estadual <span class="text-danger">*</span></label>
                                {{ Form::text('state_registration', '', array('class' => 'form-control', 'required' => '')) }}
                            </div>
                            <div class="form-group col-md-12">
                                <label for="email">E-mail <span class="text-danger">*</span></label>
                                {{ Form::email('email', '', array('class' => 'form-control', 'required' => '')) }}
                            </div>
                            <div class="form-group col-md-4">
                                <label for="id_state">Estado <span class="text-danger">*</span></label>
                                <select name="id_state" class="form-control" required="">
                                    @foreach ($states as $item)
                                        <option value="{{ $item->id }}"> {{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-8">
                                <label for="id_city">Cidade <span class="text-danger">*</span></label>
                                <select name="id_city" class="form-control" required=""></select>
                            </div>
                            <div class="form-group col-md-10">
                                <label for="address">Endereço <span class="text-danger">*</span></label>
                                {{ Form::text('address', '', array('class' => 'form-control', 'required' => '')) }}
                            </div>
                            <div class="form-group col-md-2">
                                <label for="number">Número <span class="text-danger">*</span></label>
                                {{ Form::text('number', '', array('class' => 'form-control', 'required' => '')) }}
                            </div>
                            <div class="form-group col-md-12">
                                <label for="complement">Complemento</label>
                                {{ Form::text('complement', '', array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group col-md-12">
                                <label for="cep">CEP <span class="text-danger">*</span></label>
                                {{ Form::text('cep', '', array('class' => 'form-control cep', 'required' => '')) }}
                            </div>
                            {{ Form::submit('Adicionar fornecedor', array('class' => 'btn btn-success remove-mask')) }}
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
