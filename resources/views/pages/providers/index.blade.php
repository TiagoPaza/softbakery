@extends('layouts.app')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('pages/home') }}">{{ env('APP_NAME') }} </a></li>
                    <li class="breadcrumb-item active">Fornecedores</li>
                </ol>
            </nav>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @if (Session::has('success'))
                <div class="alert bg-success text-white shadow" role="alert" style="display:block">
                    <div class="card-body">
                        <i class="fas fa-bullhorn"></i> {{ Session::get('success') }}</a>
                    </div>
                </div>
            @endif
            @if ($errors->any())
                <div class="alert bg-danger text-white shadow" role="alert" style="display:block">
                    <div class="card-body">
                        <i class="fas fa-bullhorn"></i> Houve um erro ao realizar a ação solicitada.</a>
                    </div>
                </div>
            @endif
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Fornecedores</h6>
                </div>
                <div class="card-body">
                    <p><a href="{{ url('pages/providers/create') }}" class="btn btn-info btn-rounded">+ Novo fornecedor</a></p>
                    <div class="table-responsive">
                        <table id="providers" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>NOME</th>
                                    <th>CADASTRADO EM</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($providers as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->created_at->format('d/m/Y') }} às {{ $item->created_at->format('h:m:s') }}</td>
                                    <td>
                                        <a href="{{ url('pages/providers/' . $item->id . '/edit') }}" class="btn btn-warning btn-outline btn-circle m-r-5" title="Editar">
                                            <i class="fas fa-pencil-alt" aria-hidden="true"></i>
                                        </a>
                                        {{ Form::open(['method' => 'DELETE', 'route' => ['providers.destroy', $item->id], 'style' => 'display:inline']) }}
                                        {{ Form::button('<i class="fas fa-trash-alt" aria-hidden="true"></i>', array('type' => 'submit', 'class' => 'btn btn-danger btn-outline btn-circle m-r-5', 'title' => 'Deletar')) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
