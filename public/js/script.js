// Máscaras para os campos
$(document).ready(function () {
	$('.cpf').mask('999.999.999-99');
	$('.phone').mask('(99) 9 9999-9999');
	$('.cep').mask('99999-999');
	$('.money').maskMoney();
});

// Remove máscaras ao clicar no submit
$(document).ready(function () {
	$('.remove-mask').on('click', function () {
		$('.cpf').unmask();
		$('.phone').unmask();
		$('.cep').unmask();
	});
});

// Realiza a busca das cidades com base no estado
$('select[name=id_state]').change(function () {
	let num_state = $(this).val();

	$.get('/api/cities', {
		num_state: num_state
	},
		function (src) {
			$('select[name=id_city]').empty();
			$("select[name=id_city]").append('<option value="0">Selecione</option>');

			$.each(src, function (key, value) {
				$('select[name=id_city]').append('<option value=' + value.id + '>' + value.name + '</option>');
			});
		}
	);
});


// Insere mais produtos ao clicar no botao
$(document).ready(function () {
	$('#insert-product').on('click', function () {
		let $elem = $('.form-product:last').clone(),
			index = parseInt(
				$elem.find('.increment').text()
			) + 1;

		$elem.find('.increment').text(index);
		$elem.insertAfter('.form-product:last');
	});

	$('#remove-product').on('click', function () {
		$('.form-product:last').remove();
	});
});